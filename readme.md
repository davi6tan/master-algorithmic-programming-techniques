# Master Algorithmic Programming Techniques  2016

    University of California, San Diego & Higher School of Economics
    

    Highlights

- Algorithmic Toolbox
- Data Structures
- Algorithms on Graphs
- Algorithms on Strings


## Reference

- [Master Algorithmic Programming Techniques](https://www.coursera.org/specializations/data-structures-algorithms)

`Completed September 2016`